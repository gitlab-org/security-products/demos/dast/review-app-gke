# Building a Review App for scanning with DAST

The purpose of this project is to demonstrate how to deploy a container using GKE for usage in [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/). Once configured, any new branches will deploy a container, start a review app, execute a DAST scan, then stop the environment.

## Prerequisite

Before deploying this project, a number of steps must be completed in order to prepare the Review Apps environment.

### Deploy Tokens

For GKE to be able to access the registry.gitlab.com container registry, a [Deploy Token]() must be created. 

To create a Deploy Token:

- Settings -> Repository -> Deploy Tokens
- Create a descriptive name
- Optionally set an expiration date
- Create a username of `gitlab-deploy-token`
- Click `Create deploy token`


<kbd>![deploy token](./img/deploy-token.png)</kbd>

It is recommended to give the Deploy Token the `gitlab-deploy-token` username so the CI variables will be automatically available as `$CI_DEPLOY_USER` and `$CI_DEPLOY_PASSWORD`.

After creating the deploy token, save the generated token in a secure location, as it maybe needed for troubleshooting purposes.


Once created the deploy token should available in the Deploy Token section of the Repository settings.

<kbd>![deploy token created](./img/deploy-token-created.png)</kbd>

### Configuring Kubernetes

To create a cluster and deploy an GitLab agent for Kubernetes follow [Create a Google GKE cluster](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html) guide. 

### Configuring the cluster

1. Once you have successfully deployed your cluster and installed GtiLab agent for Kubernetes, update the agent configuration to [authorize the agent to access this project](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#authorize-the-agent-to-access-your-projects). The agent is deployed together with Kubernetes cluster and its configuration is located in the project you used to deploy the cluster. 

1. Install Ingress controller to your cluster. One way to do it is to use [helm chart](https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-helm/). Once installed, make sure LoadBalancer controller service has the IP address assigned `kubectl get svc -o jsonpath='{.items[*].status.loadBalancer.ingress[*].ip}' --all-namespaces`.

1. Configure `KUBE_CONTEXT` variable under Settings -> CI/CD -> Variables. Make sure the variable is not protected.
1. Configure `KUBE_NAMESPACE` variable under Settings -> CI/CD -> Variables. You can use variable expansion and set it to the value of `$CI_PROJECT_NAME-$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG`. Make sure the variable is not protected. Set the scope of this variable to `review/*`.

## Review the gitlab-ci.yml 

The [.gitlab-ci.yml](./.gitlab-ci.yml) contains a number of necessary `kubectl` commands. Please review the comments prior to each command to ensure they apply to your specific application deployment.
